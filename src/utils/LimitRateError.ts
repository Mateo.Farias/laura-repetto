export class RateLimitError extends Error {
  constructor(msg: string) {
    super(msg);

    Object.setPrototypeOf(this, RateLimitError.prototype);
  }
}
