interface MixedMedia {
  title: string;
  imageUrl: string;
  description: string;
}

export const mixedMedias: MixedMedia[] = [
  {
    title: "Verbo Material",
    description: "Copia digital en papel de algodón de 40x50 cm.",
    imageUrl: "/images/verbo-material.webp",
  },
  {
    title: "Otro",
    description:
      "Acrílicos, pastel de tiza y tinta. Detalles de diseño digital. Impreso en papel de algodón de 40x50 cm.",
    imageUrl: "/images/otro.webp",
  },
  {
    title: "Silencio",
    description:
      "Acrílicos, pastel de tiza y tinta. Detalles de diseño digital. Impreso en papel de algodón de 40x50 cm.",
    imageUrl: "/images/silencio.webp",
  },
  {
    title: "Te veo",
    description: "Serie: Tipitos, óleo sobre madera.",
    imageUrl: "/images/te-veo.webp",
  },
  {
    title: "Trato",
    description: "Serie: Tipitos, óleo sobre madera.",
    imageUrl: "/images/trato.webp",
  },
  {
    title: "Paciencia",
    description: "Serie: Tipitos, óleo sobre madera.",
    imageUrl: "/images/paciencia.webp",
  },
  {
    title: "Sin",
    description: "Óleo, acrílico y tinta sobre tela.",
    imageUrl: "/images/sin.webp",
  },
  {
    title: "Tierra del alma",
    description: "Tinta y lejía sobre papel de algodón de 300 grs.",
    imageUrl: "/images/tierra-del-alma.webp",
  },
  {
    title: "Yaia",
    description:
      "Tinta y microfibra. Detalles de diseño digital. Impreso en papel de algodón de 40x50 cm.",
    imageUrl: "/images/yaia.webp",
  },
  {
    title: "Sombra",
    description: "Tinta y lejía sobre papel de algodón de 300 grs.",
    imageUrl: "/images/sombra.webp",
  },
  {
    title: "Jade",
    description: "Tintas, acrílicos y microfibra sobre papel sulfito 40x50 cm.",
    imageUrl: "/images/jade.webp",
  },
  {
    title: "Musa",
    description: "Copia digital en papel de algodón de 40 x 50 cm.",
    imageUrl: "/images/musa.webp",
  },
  {
    title: "Muso",
    description: "Copia digital en papel de algodón de 40 x 50 cm.",
    imageUrl: "/images/muso.webp",
  },
  {
    title: "Mal día",
    description: "Copia digital en papel de algodón de 40 x 50 cm.",
    imageUrl: "/images/mal-dia.webp",
  },
  {
    title: "Metanoia",
    description: "Copia digital en papel de algodón de 40 x 50 cm.",
    imageUrl: "/images/metanoia.webp",
  },
  {
    title: "Metanoia B",
    description:
      "Tinta, acrílico, lejía, cera de abejas y detalles de dorado a la hoja.",
    imageUrl: "/images/metanoia-b.webp",
  },
  {
    title: "Crisálida",
    description: "Tinta y lejía sobre papel de algodón de 300 grs.",
    imageUrl: "/images/crisalida.webp",
  },
  {
    title: "Dolor",
    description: "Tintas y microfibra sobre papel de algodón.",
    imageUrl: "/images/dolor.webp",
  },
  {
    title: "Bi",
    description: "Pastel de tiza sobre opalina.",
    imageUrl: "/images/bi.webp",
  },
];
