import { TextProps } from "@/components/text/Text";

export const texts: TextProps[] = [
  {
    title: "Los Uren y el oscuro secreto de los Adels",
    quote: {
      text: "Es imposible que un hombre que goza de libertad imagine lo que representa estar privado de ella",
      author: "Truman Capote",
    },
    type: "Novela",
    description:
      "Las colinas de Veggen, en Helvet, son el muro natural que separa dos mundos. En sus laderas, el milenario bosque de ginkgo será un puente entre el mundo sutil y la realidad tangible. \n Los habitantes de un lado de la colina, los Adels, lo tienen todo y guardan un secreto ancestral con el que subyugan a los Uren, los que viven del otro lado y no tienen nada. Ninna y un grupo de jóvenes Uren, guiados por las almas de sus ancestros y su sobrenatural conexión con lo místico, deciden hacerle frente a ese sistema autoritario y nefasto.\n El amor, la amistad y las creencias de cada uno se pondrán en juego en esa lucha silenciosa por la libertad. Libertad que no todos desean. Libertad que muchos creen tener.\n Laura Repetto nos adentra en un universo salpicado de toques mágicos y esotéricos, que atrapará tanto al joven lector como al adulto. Porque ya sabemos que, en el “mundillo” literario, hay historias que prefieren disfrazarse de fantasía, tan sólo para mostrarnos una posible realidad.",
    image: {
      url: "/images/uren.webp",
    },
  },
  {
    title: "Pedrito el poderoso",
    quote: {
      text: "La imaginación es tan poderosa que puede crear infinitos mundos",
      author: "",
    },
    type: "Cuento infantil",
    description:
      "Pedrito es un pequeño gran héroe que tiene el poder de transformar sus miedos en maravillosos desafíos  a superar.\n Es tierno, inteligente y hace de cada día una aventura.",
    image: {
      url: "/images/pedrito-el-poderoso.webp",
    },
  },
  {
    title: "Nina love y el globo rojo",
    quote: {
      text: "El amor es una gran red que nos conecta y une con el infinito",
      author: "",
    },
    type: "Cuento infantil",
    description:
      "Nos sumergimos en un día muy especial para Nina. Ella tiene el poder de mostrarnos  la magia de su mundo tiñendo con colores, formas y música al nuestro.",
    image: {
      url: "/images/nina-love.webp",
    },
  },
  {
    title: "Fidelia",
    quote: {
      text: "El universo te abraza, pequeña estrella, late su secreto en tu corazón: nada sería igual sin ti",
      author: "",
    },
    type: "Cuento infantil",
    description:
      "Cuando Fidelia desea algo, lo hace con intensidad y certeza. Ella nos demuestra que la fe cambia nuestra forma de ver el mundo y que la felicidad se encuentra en las cosas simples.",
    image: {
      url: "/images/fidelia.webp",
    },
  },
  {
    title: "EL NIÑO QUE QUERÍA SER PASTO",
    quote: {
      text: "Somos, naturaleza. Somos el mundo y el mundo es nosotros. Somos, hermanos",
      author: "",
    },
    type: "Cuento infantil",
    description:
      "Un día cualquiera  en la playa, un poco alejado de todo, Facu descubre lo bonita que es su vida.",
    image: {
      url: "/images/el-nino-pasto.webp",
    },
  },
];
