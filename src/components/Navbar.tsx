import Link from "next/link";

interface Props {
  links: { name: string; href: string }[];
}

const Navbar: React.FC<Props> = ({ links }: Props) => {
  return (
    <nav className="flex w-full justify-center mb-4">
      <ul className="flex min-w-max p-2">
        {links.map((link, i) => (
          <li
            key={link.name}
            className={`pt-0 pr-2.5 uppercase tracking-wide md:tracking-widest  before:pt-0 before:pr-2.5 hover:text-yellow-600 before:hover:text-black
              ${i != 0 ? 'content-["|"] before:content-["|"]' : ""} 
            `}
          >
            <Link href={link.href}>{link.name}</Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;
