import Carrousel from "../carrousel/Carrousel";

export interface BookProps {
  title: string;
  images: { url: string; alt: string }[];
  description: string[];
}

const Book: React.FC<BookProps> = ({
  title,
  description,
  images,
}: BookProps) => {
  return (
    <div className="flex flex-col w-full px-12 gap-1 md:gap-3 mb-3">
      <div className="flex w-full justify-center gap-1">
        {title.split("").map((l, i) =>
          l == " " ? (
            <br />
          ) : (
            <p
              key={i}
              className="bg-gray-500 px-1 text-white lowercase md:text-xl"
            >
              {l}
            </p>
          )
        )}
      </div>
      <div className="h-80">
        <Carrousel>
          {images.map((image) => (
            <img
              key={image.alt}
              className="h-full"
              src={image.url}
              alt={image.alt}
            />
          ))}
        </Carrousel>
      </div>
      <div className="flex flex-col gap-2 md:px-6">
        {description.map((des, i) => (
          <p className="italic tracking-tighter leading-4" key={i}>
            {des}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Book;
