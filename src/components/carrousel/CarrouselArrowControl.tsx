interface CarrouselArrowControlProps {
  scroll: Function;
  direction: "left" | "right";
}

const CarrouselArrowControl: React.FC<CarrouselArrowControlProps> = ({
  scroll,
  direction,
}: CarrouselArrowControlProps) => {
  const isPositionLeft: boolean = direction == "left";
  const scrollValue = isPositionLeft ? -200 : 200;
  const positionAttributes = isPositionLeft ? "-left-8" : "top-0 -right-8";
  return (
    <div
      className={`flex absolute ${positionAttributes} items-center h-full`}
      onClick={() => scroll(scrollValue)}
    >
      {isPositionLeft ? (
        <div className="cursor-pointer">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5 8.25 12l7.5-7.5"
            />
          </svg>
        </div>
      ) : (
        <div className="cursor-pointer">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="m8.25 4.5 7.5 7.5-7.5 7.5"
            />
          </svg>
        </div>
      )}
    </div>
  );
};

export default CarrouselArrowControl;
