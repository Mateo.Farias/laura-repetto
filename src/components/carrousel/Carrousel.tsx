"use client";
import { MouseEvent, useRef, useState } from "react";
import CarrouselArrowControl from "./CarrouselArrowControl";

interface CarrouselProps {
  children: React.ReactNode;
}

const Carrousel: React.FC<CarrouselProps> = ({ children }: CarrouselProps) => {
  const scrollRef = useRef<HTMLDivElement>(null);
  const [isScrolling, setIsScrolling] = useState(false);
  const [blockChildren, setBlockChildren] = useState(false);
  const [position, setPosition] = useState({ startX: 0, scrollLeft: 0 });

  const scroll = (scrollOffset: number) => {
    if (scrollRef.current != null) scrollRef.current.scrollLeft += scrollOffset;
  };

  const handleMouseDown = (e: MouseEvent) => {
    setIsScrolling(true);
    setPosition({
      startX: e.pageX - scrollRef.current!.offsetLeft,
      scrollLeft: scrollRef.current!.scrollLeft,
    });
  };

  const handleMouseUp = (_: MouseEvent) => {
    setIsScrolling(false);
    setBlockChildren(false);
  };

  const handleMouseLeave = () => {
    setIsScrolling(false);
    setBlockChildren(false);
  };

  const handleMouseMove = (e: MouseEvent) => {
    if (!isScrolling) return;
    e.preventDefault();

    const mouseMovementFromOrigin = position.startX - e.clientX;
    if (mouseMovementFromOrigin > 10 || mouseMovementFromOrigin < -10)
      setBlockChildren(true);

    const eventPosition = e.pageX - scrollRef.current!.offsetLeft;
    const slide = eventPosition - position.startX;

    scrollRef.current!.scrollLeft = position.scrollLeft - slide;
  };

  return (
    <div className="relative w-full h-full select-none">
      <CarrouselArrowControl direction="left" scroll={scroll} />
      <div
        className="relative gap-2 flex flex-row h-full py-2.5 overflow-x-scroll scroll-smooth scrollbar-hide"
        ref={scrollRef}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onMouseMove={handleMouseMove}
        onMouseLeave={handleMouseLeave}
      >
        {children}
      </div>
      <CarrouselArrowControl direction="right" scroll={scroll} />
    </div>
  );
};

export default Carrousel;
