interface Props {
  imageUrl: string;
  title: string;
  description: string;
}

const Card: React.FC<Props> = ({ title, description, imageUrl }: Props) => {
  return (
    <div className="flex flex-col justify-between w-72 p-1 shadow-[0_3px_10px_rgb(0,0,0,0.2)]">
      <img className="h-full" src={imageUrl} alt={title} />
      <div className="mt-1 px-1.5">
        <h5 className="uppercase">{title}</h5>
        <p className="leading-tight tracking-tighter">{description}</p>
      </div>
    </div>
  );
};

export default Card;
