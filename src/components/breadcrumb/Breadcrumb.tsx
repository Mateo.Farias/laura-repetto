"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

type TGaleryPath = "galery" | "books" | "mixed-media" | "texts";

const pathsTranslations = {
  galery: "Galeria",
  books: "Libros de artista",
  "mixed-media": "Mixed-Media",
  texts: "Textos",
};

const getPathTranslation = (link: TGaleryPath) => {
  return pathsTranslations[link];
};

const Breadcrumb = () => {
  const paths = usePathname();
  const pathNames = paths.split("/").filter((path) => path);

  if (pathNames.length == 1) return <></>;

  return (
    <div className="">
      <ul className="flex gap-3">
        {pathNames.map((link, index) => {
          let href = `/${pathNames.slice(0, index + 1).join("/")}`;
          return (
            <li
              key={href}
              className={`hover:text-yellow-600 after:pl-2 after:hover:text-black
                ${pathNames.length - 1 != index ? "after:content-['>']" : ""}`}
            >
              <Link href={href}>{getPathTranslation(link as TGaleryPath)}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Breadcrumb;
