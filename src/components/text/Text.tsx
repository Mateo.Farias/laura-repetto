interface Quote {
  text: string;
  author: string;
}

interface TextImage {
  url: string;
  epigraph?: string;
}

export interface TextProps {
  title: string;
  quote: Quote;
  description: string;
  type: string;
  link?: string;
  image?: TextImage;
}

const Text: React.FC<TextProps> = ({
  title,
  description,
  quote,
  type,
  image,
  link,
}: TextProps) => {
  return (
    <div className="flex flex-col gap-4 px-3">
      <h2 className="text-xl font-bold">
        {title}
        <i className="font-thin text-sm"> - {type}</i>
      </h2>
      <blockquote className="text-md leading-tight">
        <i>"{quote.text}".</i> {quote.author}
      </blockquote>
      {Boolean(image) && <img src={image?.url} />}
      <p className="text-justify leading-tight tracking-tighter whitespace-pre-line md:leading-snug">
        {description}
      </p>
    </div>
  );
};

export default Text;
