import Image from "next/image";
import brandIcon from "../../public/images/brand.webp";
import Link from "next/link";

const Logo = () => {
  return (
    <div className="relative mx-auto">
      <Link href="/">
        <h1 className="text-transform: uppercase text-4xl">laura repetto</h1>
        <Image
          alt="brand-icon"
          src={brandIcon}
          width={80}
          height={80}
          className="absolute -right-9 -top-7 -z-10"
        />
      </Link>
    </div>
  );
};

export default Logo;
