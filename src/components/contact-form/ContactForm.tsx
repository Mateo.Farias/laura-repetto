"use client";
import { FormEventHandler, useState } from "react";
import Button from "../ui/Button";
import Input from "../ui/Input";

interface IFormData {
  name: string;
  email: string;
  subject: string;
  message: string;
}

const ContactForm: React.FC = () => {
  const [isSendingForm, setIsSendingForm] = useState(false);
  const [formSuccessfullySended, setFormSuccessfullySended] = useState<
    null | boolean
  >(null);

  const handleFormSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    const submitableData: IFormData = {
      name: "",
      email: "",
      subject: "",
      message: "",
    };
    const formData = new FormData(e.currentTarget);
    e.preventDefault();
    setIsSendingForm(true);

    for (const key in submitableData) {
      if (key in submitableData)
        submitableData[key as keyof IFormData] = formData.get(key) as string;
    }

    fetch("/api/mail", {
      method: "POST",
      body: JSON.stringify(submitableData),
    })
      .then((res) => {
        if (res.status == 200) {
          setFormSuccessfullySended(true);
        } else if (res.status == 429) {
          alert("Alcanzaste el limite de solicitudes.");
        } else {
          setFormSuccessfullySended(false);
        }
      })
      .finally(() => {
        setIsSendingForm(false);
      });
  };

  return (
    <form
      onSubmit={handleFormSubmit}
      className="flex flex-col self-center w-full max-w-96 gap-4 px-2"
    >
      <Input type="text" label="Nombre" name="name" required />
      <Input type="email" label="Email" name="email" required />
      <Input type="text" label="Asunto" name="subject" required />
      <Input type="textarea" label="Mensaje" name="message" required />
      <Button
        name="Enviar"
        type="submit"
        disabled={isSendingForm || !!formSuccessfullySended}
        isLoading={isSendingForm}
      />
      {formSuccessfullySended != null && (
        <p className={`${formSuccessfullySended == false && "text-red-500"}`}>
          {formSuccessfullySended
            ? "Gracias por comunicarte conmigo! En breve te estare contestando."
            : "Ocurrio un error al enviar la solicitud, intenta nuevamente mas tarde."}
        </p>
      )}
    </form>
  );
};

export default ContactForm;
