import { HTMLInputTypeAttribute } from "react";

interface Props {
  type: HTMLInputTypeAttribute;
  name: string;
  label: string;
  placeholder?: string;
  required?: boolean;
}

const Input: React.FC<Props> = ({
  name,
  label,
  type,
  required,
  placeholder = "",
}: Props) => {
  return (
    <div>
      <label className="block text-lg tracking-tight">
        {label} {required && <small className="text-xs">(requerido)</small>}
      </label>
      {type == "textarea" ? (
        <textarea
          className={`appearance-none rounded-none block w-full border-0 pl-2 py-2 text-gray-900 placeholder:text-gray-400
          ring-1 ring-inset ring-gray-300 
          focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500 
          sm:text-sm sm:leading-6
        `}
          name={name}
          placeholder={placeholder}
        />
      ) : (
        <input
          className={`appearance-none rounded-none block w-full border-0 pl-2 py-2 text-gray-900 placeholder:text-gray-400
          ring-1 ring-inset ring-gray-300  
          focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500 
          sm:text-sm sm:leading-6
        `}
          type={type}
          name={name}
          placeholder={placeholder}
        />
      )}
    </div>
  );
};

export default Input;
