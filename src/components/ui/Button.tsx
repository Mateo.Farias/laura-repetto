import Spinner from "./Spinner";

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
  isLoading?: boolean;
}

const Button: React.FC<Props> = ({
  type,
  name,
  disabled,
  isLoading = false,
}: Props) => {
  return (
    <button
      className={`border p-2 bg-gray-800 ${
        isLoading ? "" : "hover:bg-gray-500"
      } font-bold text-white`}
      type={type}
      disabled={disabled}
    >
      {isLoading ? <Spinner /> : name}
    </button>
  );
};

export default Button;
