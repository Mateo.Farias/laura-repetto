import Navbar from "@/components/Navbar";
import Logo from "@/components/Logo";
import type { Metadata } from "next";
import localFont from "next/font/local";
import "./globals.css";

const daisyWheel = localFont({
  src: "../../public/fonts/daisywhl.otf",
  variable: "--font-daisywheel",
});

export const metadata: Metadata = {
  title: "Laura Repetto",
  description: "Conoce mi arte",
  icons: {
    icon: "/images/circle.webp",
  },
};

const links = [
  { href: "/about", name: "Sobre mi" },
  { href: "/galery", name: "Galeria" },
  { href: "/contact", name: "Contacto" },
];

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="es">
      <body className={daisyWheel.variable}>
        <main className="flex min-h-screen flex-col items-center pb-10">
          <div className="flex flex-col flex-1 pt-8 gap-4 w-full max-w-screen-md">
            <Logo />
            <Navbar links={links} />
            {children}
          </div>
        </main>
      </body>
    </html>
  );
}
