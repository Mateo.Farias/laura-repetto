import rateLimit from "@/middleware/rateLimiter";
import { RateLimitError } from "@/utils/LimitRateError";
import { NextRequest, NextResponse } from "next/server";
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";

const senderEmail = process.env.EMAIL;
const senderEmailPassword = process.env.EMAIL_PASSWORD;

const transport = nodemailer.createTransport({
  service: "hotmail",
  auth: {
    user: senderEmail,
    pass: senderEmailPassword,
  },
});

export async function POST(request: NextRequest) {
  try {
    rateLimit(request);
    const { name, email, subject, message } = await request.json();
    const mailOptions: Mail.Options = {
      from: senderEmail,
      to: senderEmail,
      subject: `${subject}`,
      text: `Mensaje de: ${name} - mail: ${email} | ${subject} \n ${message}`,
    };

    await transport.sendMail(mailOptions);
    return NextResponse.json("ok");
  } catch (error) {
    if (error instanceof RateLimitError) {
      console.log(error.message);
      return NextResponse.json("Rate limit reached", { status: 429 });
    } else {
      console.log("Error enviando mail", error);
      return NextResponse.json("Error while sending mail", { status: 500 });
    }
  }
}
