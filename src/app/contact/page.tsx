import ContactForm from "@/components/contact-form/ContactForm";

export default function Contact() {
  return <ContactForm />;
}
