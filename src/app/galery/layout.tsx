import Breadcrumb from "@/components/breadcrumb/Breadcrumb";

export default function CategoryLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <section className="flex flex-col flex-1 gap-6 px-2">
      <Breadcrumb />
      {children}
    </section>
  );
}
