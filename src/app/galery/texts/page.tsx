import Text from "@/components/text/Text";
import { texts } from "@/constants/texts";

export default function Texts() {
  return (
    <div className="flex flex-col gap-10">
      {texts.map(({ title, description, quote, type, image, link }, i) => (
        <>
          <Text
            key={title}
            title={title}
            description={description}
            quote={quote}
            type={type}
            image={image}
            link={link}
          />
          {i < texts.length - 1 && (
            <div className="px-12">
              <hr className="border-t-[1px] border-gray-600" />
            </div>
          )}
        </>
      ))}
    </div>
  );
}
