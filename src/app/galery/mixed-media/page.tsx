import Card from "@/components/card/Card";
import { mixedMedias } from "@/constants/mixed-media";

export default function MixedMedia() {
  return (
    <div className="flex flex-wrap justify-evenly gap-y-12">
      {mixedMedias.map(({ title, description, imageUrl }) => (
        <Card
          key={title}
          title={title}
          imageUrl={imageUrl}
          description={description}
        />
      ))}
    </div>
  );
}
