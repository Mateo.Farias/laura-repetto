import Link from "next/link";

const galeryOptions = [
  {
    name: "libros de artista",
    url: "/galery/books",
    image: "/images/books-background.webp",
  },
  {
    name: "mixed-media",
    url: "/galery/mixed-media",
    image: "/images/mixed-media-background.webp",
  },
  {
    name: "textos",
    url: "/galery/texts",
    image: "/images/texts-background.webp",
  },
];

export default function Galery() {
  return (
    <section className="flex flex-col flex-1 gap-4 p-2">
      {galeryOptions.map((option) => (
        <Link
          className={`select-none rounded-sm cursor-pointer md:h-36 lg:min-h-64 flex flex-auto items-center justify-center bg-no-repeat bg-cover bg-center`}
          passHref
          href={option.url}
          key={option.name}
          style={{ backgroundImage: `url(${option.image})` }}
        >
          <div className="flex align-middle gap-1">
            {option.name
              .split("")
              .map((letter) =>
                letter == " " ? (
                  <br />
                ) : (
                  <p className="bg-white px-1 md:text-xl uppercase">{letter}</p>
                )
              )}
          </div>
        </Link>
      ))}
    </section>
  );
}
