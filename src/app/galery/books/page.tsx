import Book from "@/components/book/Book";
import { books } from "@/constants/books";

export default function Books() {
  return books.map(({ title, images, description }) => (
    <Book key={title} title={title} images={images} description={description} />
  ));
}
