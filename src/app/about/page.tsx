import Image from "next/image";
import BioImage from "../../../public/images/BioPhoto.webp";

export default function Bio() {
  return (
    <div className="flex flex-col md:flex-row w-full gap-5">
      <Image className="md:max-w-80" src={BioImage} alt="BioPic" priority />
      <div className="md:py-5 px-6 flex flex-col justify-between">
        <p className=" md:text-left tracking-tighter md:leading-relaxed text-md md:text-md leading-normal">
          Nací en 1972 en Buenos Aires, Argentina.
          <br />
          Soy escritora y artista visual.
          <br />
          Me desempeñé como publicista, diseñadora y ambientadora.
          <br />
          Viajar, vivir la naturaleza, fotografiarla, la mística, la filosofía y
          una mirada curiosa del mundo me nutren e inspiran.
          <br />
          En la actualidad escribo novelas, poesías, cuentos infantiles y hago
          libros de artista.
          <br />
          Encuentro en ellos la unión perfecta entre imagen y palabra: unen
          mundos, nos conectan con el otro y con nosotros mismos.
        </p>
        <p className="italic font-bold pt-5">
          "El arte y la palabra son fuego <br /> que transmuta".
        </p>
      </div>
    </div>
  );
}
