export default function Home() {
  return (
    <div className="relative">
      <div className="absolute h-full w-full -z-10 bg-[url('/images/fondo.jpg')] bg-cover bg-center bg-no-repeat "></div>
      <p className="py-8 text-center">
        Observo lo que me rodea.
        <br />
        Pienso que para crear
        <br />
        hay que saber mirar.
        <br />
        Busco vivir en serendipia.
        <br />
        Creo que toda historia
        <br />
        tiene un comienzo:
        <br />
        el cruce de una mirada,
        <br />
        alguien que camina por la calle,
        <br />
        un pájaro en la ventana,
        <br />
        una charla de bar.
        <br />
        Entran sin permiso,
        <br />
        pasan del lápiz al papel
        <br />
        de mi cuarderno de notas
        <br />
        y comienzan a latir
        <br />
        en ese limbo de ideas
        <br />
        hasta convertirse en realidad.
        <br />
        El arte es espíritu
        <br />
        expresado en materia.
        <br />
        El Libro de Artista
        <br />
        es el medio por el cual
        <br />
        el mío halló la comunión
        <br />
        entre imágenes y palabras.
        <br />
        Espero que quien entre
        <br />
        en contacto con mi obra
        <br />
        lo haga propio,
        <br />
        lo transforme
        <br />
        y reinicie
        <br />
        la cadena creativa.
      </p>
    </div>
  );
}
