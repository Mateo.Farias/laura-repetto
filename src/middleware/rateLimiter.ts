import { RateLimitError } from "@/utils/LimitRateError";
import { NextRequest } from "next/server";

const rateLimitMap = new Map();
const ONE_HOUR = 60 * 60 * 1000;

export default function rateLimit(req: NextRequest) {
  const ip = req.headers.get("x-forwarded-for");
  console.log("ip", ip);
  const limit = 2; // Limiting requests to 5 per minute per IP
  const windowMs = ONE_HOUR; // 1 minute

  if (!rateLimitMap.has(ip)) {
    rateLimitMap.set(ip, {
      count: 0,
      lastReset: Date.now(),
    });
  }

  const ipData = rateLimitMap.get(ip);

  if (Date.now() - ipData.lastReset > windowMs) {
    ipData.count = 0;
    ipData.lastReset = Date.now();
  }

  if (ipData.count >= limit) {
    throw new RateLimitError(`Rate limit reached for ip: ${ip}`);
  }

  ipData.count += 1;
}
